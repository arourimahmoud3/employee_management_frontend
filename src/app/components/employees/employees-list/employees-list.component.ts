import { Component, OnInit } from '@angular/core';
import { Employee } from 'src/app/models/employee.model';

@Component({
  selector: 'app-employees-list',
  templateUrl: './employees-list.component.html',
  styleUrls: ['./employees-list.component.css']
})
export class EmployeesListComponent implements OnInit {

  employees: Employee[] = [
   { 
    id: 'A1001',
    name: 'Mahmoud AROURI',
    email: 'mahmoud.arouri@gmail.com',
    phone: 98765432,
    salary: 1800,
    department: 'FocusLab'

   },

   {
    id: 'A1002',
    name: 'Marwen AROURI',
    email: 'marwen.arouri@gmail.com',
    phone: 23456788,
    salary: 1800,
    department: 'SAP Support'

   }
  ];

  constructor() {

  }

  ngOnInit(): void {
  
  }

}
